$MOD186
NAME TIMER
; Main program for uPD70208 microcomputer system
;
; Author: 	Dr Tay Teng Tiow
; Address:     	Department of Electrical Engineering 
;         	National University of Singapore
;		10, Kent Ridge Crescent
;		Singapore 0511.	
; Date:   	6th September 1991
;
; This file contains proprietory information and cannot be copied 
; or distributed without prior permission from the author.
; =========================================================================

public	serial_rec_action, timer2_action
extrn	print_char:far, print_2hex:far, iodefine:far
extrn   set_timer2:far

STACK_SEG	SEGMENT
		DB	256 DUP(?)
	TOS	LABEL	WORD
STACK_SEG	ENDS


DATA_SEG	SEGMENT
	array           DB      '0','1','2','3','4','5','6','7','8','9'
	TIMER0_MESS	DB	10,13,'TIMER2 INTERRUPT    '
	T_COUNT		DB	2FH
	T_COUNT_SET	DB	2FH
	REC_MESS	DB	10,13,'Period of timer0 =   
	


DATA_SEG	ENDS


CODE_SEG	SEGMENT

	PUBLIC		START

ASSUME	CS:CODE_SEG, SS:STACK_SEG

START:
;initialize stack area
		MOV	AX,STACK_SEG		
		MOV	SS,AX
		MOV	SP,TOS

; Initialize the on-chip pheripherals
		CALL	FAR PTR	IODEFINE
		


; ^^^^^^^^^^^^^^^^^  Start of User Main Routine  ^^^^^^^^^^^^^^^^^^
    call set_timer2
                 STI


		
		MOV AL, 82H       ;mode 0, A - out, B-in ;changed value of CW from 82H
		MOV DX, 8003H
		OUT DX, AL	;send the control word
		MOV BL, 00H	;initialize BL for key code
		XOR AX, AX	;clear al flags
		MOV DX, 8002H  ;port A address to DX
		OUT DX, AL	;ground all rows
		MOV DX, 8001H	;Port B address to DX


NEXT:    
;code

                WAIT:	IN AL, DX	;read all columns
		AND AL, 07H	;Mask data lines D7-D3
		
		CMP AL, 07H ;any key pressed?
		JZ WAIT		;if not, wait till key press
		
		CALL DEBOUNCE ;wait for 10ms if key press
		
		MOV AL, 07FH	;load data byte to ground a row;
		MOV BH, 04H	;set row counter
NXTROW:		ROL AL, 01H       ;rotate AL to ground next row
		MOV CH, AL	;save data byte to ground next row
		MOV DX, 8002H	;port B address to DX; Changed from 8000H (port A)
		OUT DX, AL	;ground one of the rows

		MOV DX, 8001H	;port C address to DX  ; cHANGED FROM port B
		IN AL, DX	;read input port for key closure
		AND AL, 07H	;Mask D4-D7
		MOV CL, 03H	;set column counter
		
		
NXTCOL:	RCR AL, 01H ;move D0 to CF		  	
		JNC CODEKY	;key closure is found, if CF=0
		INC BL		;inc BL for next binary key code
		DEC CL		;dec column counter if no key closure
		JNZ NXTCOL	;check for key closure in next column
		MOV AL, CH	;Load data byte to ground next row
		DEC BH		;if no key closure found in all columns 					 in this row, go to ground next row
		JNZ NXTROW	;go back to ground next row
		JMP WAIT	;back to check for key closure again
CODEKY:
		MOV AL, BL	;key code is transferred to AL
                XOR BL,BL             ;BX is now used for indexing the array
		CMP AL,10;
		JL NUMBERS
CHARACTERS:	CMP AL,10
		JE HASH
		JG ASTERISK
	
HASH:		MOV AL,'#'
          	JMP RETPOINT

ASTERISK	MOV AL,'*'
		JMP RETPOINT					
		

NUMBERS:	
		MOV BL,AL ; move contents of AL to Bl
                XOR BH,BH
		MOV AL,array[BX] ; Stores character in AL (?)
		XOR AH,AH
		JMP RETPOINT
              
		
RETPOINT:	CALL	FAR PTR PRINT_CHAR
		
		
		
DEBOUNCE:	PROC	NEAR
			PUSH	CX

Option1:	MOV CX, 094Ch ; 2380 dec
BACK:		NOP	  ; 3 clocks
			LOOP BACK; 18 clocks

			POP CX
			RET
DEBOUNCE	ENDP

 JMP NEXT

; ^^^^^^^^^^^^^^^ End of User main routine ^^^^^^^^^^^^^^^^^^^^^^^^^


SERIAL_REC_ACTION	PROC	FAR
		PUSH	CX
		PUSH 	BX
		PUSH	DS

		MOV	BX,DATA_SEG		;initialize data segment register
		MOV	DS,BX

		CMP	AL,'<'
		JNE	S_FAST

		INC	DS:T_COUNT_SET
		INC	DS:T_COUNT_SET

		JMP	S_NEXT0
S_FAST:
		CMP	AL,'>'
		JNE	S_RET

		DEC	DS:T_COUNT_SET
		DEC	DS:T_COUNT_SET

S_NEXT0:
		MOV	CX,22			;initialize counter for message
		MOV	BX,0

S_NEXT1:	MOV	AL,DS:REC_MESS[BX]	;print message
		call	FAR ptr print_char
		INC	BX
		LOOP	S_NEXT1

		MOV	AL,DS:T_COUNT_SET	;print current period of timer0
		CALL	FAR PTR PRINT_2HEX
S_RET:
		POP	DS
		POP	BX
		POP	CX
		RET
SERIAL_REC_ACTION	ENDP



TIMER2_ACTION	PROC	FAR
		PUSH	AX
		PUSH	DS
		PUSH	BX
		PUSH	CX

		MOV	AX,DATA_SEG
		MOV	DS,AX
	
		DEC	DS:T_COUNT
		JNZ	T_NEXT1
		MOV	AL,DS:T_COUNT_SET
		MOV	DS:T_COUNT,AL

		MOV	CX,20
		MOV	BX,0H
T_NEXT0:
		MOV	AL,DS:TIMER0_MESS[BX]
		INC	BX
		CALL 	FAR PTR PRINT_CHAR
		LOOP	T_NEXT0

T_NEXT1:	
		POP	CX
		POP	BX
		POP	DS
		POP 	AX
		RET
TIMER2_ACTION	ENDP


CODE_SEG	ENDS
END
